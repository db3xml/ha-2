#!/bin/bash
 
# Path to this script
FILE="${BASH_SOURCE[0]}"
while [ -h "$FILE" ] ; do
  SRC="$(readlink "$FILE")"
  FILE="$( cd -P "$(dirname "$FILE")" && \
           cd -P "$(dirname "$SRC")" && pwd )/$(basename "$SRC")"
done
BX="$( cd -P "$(dirname "$FILE")/.." && pwd )"
BXCORE="$( cd -P "$BX/../basex" && pwd )"
 
# API, core, and library classes
CP="$BX/BaseX.jar$(printf ":%s" "$BX/BaseX.jar" "$BX/lib/"*.jar "$BXCORE/target/classes" "$BXCORE/lib/"*.jar)"
 
# Options for virtual machine
VM=-Xmx512m
 
# Run code
java -cp "$CP" $VM org.basex.BaseXHTTP "$@"