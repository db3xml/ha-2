package de.uni_frankfurt.cs.acoli.db3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


public class BaseXStore implements Store {

	private String baseUrl;
	private String database;

	public BaseXStore(String baseUrl, String database) {
		this.baseUrl = baseUrl;
		this.database = database;
	}
	
	private String getQueryURI() {
		return this.baseUrl+"/"+database;
	}
	
	private String buildXMLQuery(String query) {
		String xml = "<query xmlns='http://basex.org/rest'>"
				   + "<text><![CDATA["+query+"]]></text>"
			   	   + "<parameter name=\"wrap\" value=\"true\"/>"
				   + "</query>";
		return xml;
	}
	
	@Override
	public Result query(String query) {
		try {
			String xmlQuery = this.buildXMLQuery(query);
			Request r = Request.Post(this.getQueryURI());
			r.body(new StringEntity(xmlQuery, ContentType.APPLICATION_XML));
			Response res = r.execute();
			Document doc = this.parseXMLResponse(res);
			return new XMLResult(doc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Result queryURL(String path) {
		try {
			String uri = this.getQueryURI()+path;
			Request r = Request.Get(uri);
			Response res = r.execute();
			Document doc = this.parseXMLResponse(res);
			return new XMLResult(doc);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	protected Document parseXMLResponse(Response res) {
		DocumentBuilderFactory factory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			HttpResponse r = res.returnResponse();
			InputStream content = r.getEntity().getContent();
			int code = r.getStatusLine().getStatusCode();
			if(code == 200) {
				Document doc = builder.parse(content);
				return doc;
			}
			else {
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
		        StringBuilder out = new StringBuilder();
		        String line;
		        while ((line = reader.readLine()) != null) {
		            out.append(line);
		        }
		        System.err.println(out.toString());   //Prints the string content read from input stream
		        reader.close();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean createDatabase() {
		try {
			Request r = Request.Put(this.getQueryURI()+"?chop=false");
			Response res = r.execute();
			int code = res.returnResponse().getStatusLine().getStatusCode();
			System.out.println(code);
			if(code == 201) {
				return true;
			}
			else {
				return false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean deleteDatabase() {
		try {
			Request r = Request.Delete(this.getQueryURI());
			Response res = r.execute();
			int code = res.returnResponse().getStatusLine().getStatusCode();
			if(code == 200) {
				return true;
			}
			else {
				return false;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean storeXML(String filename, Document xml) {
		try {
			Request r = Request.Put(getQueryURI()+"/"+filename+"?chop=false");
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(xml), new StreamResult(writer));
			String output = writer.getBuffer().toString();
			r.body(new StringEntity(output, ContentType.APPLICATION_XML));
			Response res = r.execute();
			int code = res.returnResponse().getStatusLine().getStatusCode();
			if(code == 201) {
				return true;
			}
			else {
				return false;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
}
