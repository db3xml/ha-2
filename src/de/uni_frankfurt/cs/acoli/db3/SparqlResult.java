package de.uni_frankfurt.cs.acoli.db3;

import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;

/**
 * Sparql Result Class
 * @author alex
 *
 */
public class SparqlResult implements Result {

	private ResultSet resultSet;
	
	/** constructor of the class
	 * 
	 * @param resultSet
	 */
	public SparqlResult(ResultSet resultSet) {
		this.resultSet = resultSet;
	}
	
	/**
	 * returns the Result as String
	 */
	@Override
	public String getResultAsString() {
		return ResultSetFormatter.asText(resultSet);
	}
	
	/**
	 * returns the ResultSet
	 * @return ResultSet
	 */
	public ResultSet getResultSet() {
		return this.resultSet;
	}

}
