package de.uni_frankfurt.cs.acoli.db3;

/**
 * Reslt interface
 *
 */
public interface Result {

	public String getResultAsString();
	
}
