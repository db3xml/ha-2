package de.uni_frankfurt.cs.acoli.db3;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

/**
 * 
 *
 */
public class XMLResult implements Result {

	private Document document;
	
	/**
	 * 
	 * @param document
	 */
	public XMLResult(Document document) {
		this.document = document;
	}
	
	/**
	 * 
	 * @return
	 */
	public Document getDocument() {
		return this.document;
	}
	
	/**
	 * 
	 */
	@Override
	public String getResultAsString() {
		try {
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(this.document), new StreamResult(writer));
			String output = writer.getBuffer().toString();
			return output;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
