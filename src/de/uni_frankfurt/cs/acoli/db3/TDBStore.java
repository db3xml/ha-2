package de.uni_frankfurt.cs.acoli.db3;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.tdb.TDBFactory;

/**
 * TDBStore class
 * @author alex
 *
 */
public class TDBStore implements DataStore {

	private Dataset dataset;
	
	/**
	 * 
	 * @param path
	 */
	public TDBStore(String path) {
		this.dataset = this.createDataset(path);
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	protected Dataset createDataset(String path) {
		return TDBFactory.createDataset(path) ;
	}
	
	/**
	 * 
	 */
	public Result query(String query) {        
        Query q = QueryFactory.create(query) ;
        QueryExecution qexec = QueryExecutionFactory.create(query, dataset) ;
        ResultSet results = qexec.execSelect() ;
        return new SparqlResult(results);
	}
	
	/**
	 * 
	 * @param path
	 * @return
	 */
	public Model importTurtle(String path) {
		Model model = this.dataset.getDefaultModel().read(path, "TURTLE");
		return model;
	}
	
	/**
	 * 
	 * @param uri
	 * @return
	 */
	public Model getModel(String uri) {
		if(this.dataset.containsNamedModel(uri)) {
			return this.dataset.getNamedModel(uri);
		}
		else {
			Model model = ModelFactory.createDefaultModel();
			this.dataset.addNamedModel(uri, model);
			return model;
		}
	}
	
	public Dataset getDataset() {
		return this.dataset;
	}
	
}
