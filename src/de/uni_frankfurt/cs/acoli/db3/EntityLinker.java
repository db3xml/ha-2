package de.uni_frankfurt.cs.acoli.db3;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.xml.transform.TransformerConfigurationException;

import org.apache.jena.riot.RiotException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryParseException;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.ReifiedStatement;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;

public class EntityLinker {

	private BaseXStore articles;
	private TDBStore jrcNames;
	private HashSet<String> knownURIs;
	private BaseXStore linkedArticles;
	private SparqlWebStore dbpedia;
	private TDBStore localStore;
	private ArrayList<Tuple> tuples;
	private ArrayList<Triple> triples;

	public EntityLinker() {
		this.articles = new BaseXStore("http://localhost:8984/rest", "articles");
		this.linkedArticles = new BaseXStore("http://localhost:8984/rest",
				"LinkedArticles");
		this.jrcNames = new TDBStore("jrcnames");
		this.localStore = new TDBStore("local");
		this.dbpedia = new SparqlWebStore("http://dbpedia.org/sparql");		
		this.knownURIs = new HashSet<String>();
		this.tuples = new ArrayList<Tuple>();
		this.triples = new ArrayList<Triple>();
	}
	
	public void link() {
		XMLResult xmlResult = (XMLResult) this.articles.queryURL("/");
		Document dom = xmlResult.getDocument();

		this.linkedArticles.deleteDatabase();
		this.linkedArticles.createDatabase();

		NodeList articles = dom.getElementsByTagName("rest:resource");
		for (int i = 0, len = articles.getLength(); i < len; i++) {
			Node article = articles.item(i);
			String articleName = article.getTextContent().trim();
			this.processArticle(articleName);
		}
	}

	protected void processArticle(String articleName) {
		XMLResult xmlResult = (XMLResult) this.articles.queryURL("/"
				+ articleName);
		Document dom = xmlResult.getDocument();

		NodeList sentences = dom.getElementsByTagName("s");
		for (int node = 0; node < sentences.getLength(); node++) {
			Node sentence = sentences.item(node);
			NodeList childs = sentence.getChildNodes();
			int lastIndex = -1;

			int startIndex = -1;
			String lastType = "";
			String name = "";
			ArrayList<NamedEntity> namedEntities = new ArrayList<NamedEntity>();

			for (int child = 0; child < childs.getLength(); child++) {
				if (childs.item(child).hasAttributes()) {
					NamedNodeMap attributes = childs.item(child)
							.getAttributes();
					Node indexAttribute = attributes.getNamedItem("index");
					Node typeAttribute = attributes.getNamedItem("type");
					int index = Integer.valueOf(indexAttribute.getNodeValue());
					String type = typeAttribute.getNodeValue();
					String childName = childs.item(child).getTextContent();

					if ((lastIndex == -1 || (lastIndex + 1) == index)
							&& (lastType.equals("") || lastType.equals(type))) {
						if (startIndex == -1)
							startIndex = index;
						name += childName + " ";
					} else {
						NamedEntity ne = new NamedEntity(name.trim(),
								startIndex, lastType);
						namedEntities.add(ne);
						name = "";
						startIndex = index;
						name += childName + " ";
					}
					lastIndex = index;
					lastType = type;
				}
			}
			if (name.trim().length() > 0) {
				namedEntities.add(new NamedEntity(name, startIndex, lastType));
			}

			for (NamedEntity ne : namedEntities) {
				ArrayList<NamedEntity> matchedEntities = this.matchEntities(ne);

				for(NamedEntity matchedNe : matchedEntities) {
					this.storeDataInXML(sentence, matchedNe);
				}
			}
			
			this.linkToDBpedia(sentence);
		}
		this.linkedArticles.storeXML(articleName, dom);

	}
	
	private void linkToDBpedia(Node sentence) {
		NodeList children = sentence.getChildNodes();
		for(int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			Element element;
			try {
				element = (Element) child;
			}
			catch(Exception e) {
				continue;
			}
			try {
				String content = element.getTextContent();
				String dbpediaURL = null;
				System.out.println("Attempting to find link to DBpedia from JRCNames for "+content+"...");
				if(element.getTagName().equals("nm")) {
					String resourceName = null;
					if(!element.hasAttribute("dbpedia")) {
						if(element.hasAttribute("jrc-names")) {
							String jrcNamesUrl = element.getAttribute("jrc-names");
							SparqlResult result = (SparqlResult) this.jrcNames.query("PREFIX owl: <http://www.w3.org/2002/07/owl#> SELECT ?dbpedia WHERE {<"+jrcNamesUrl+"> owl:sameAs ?dbpedia}");
							ResultSet resultSet = result.getResultSet();
							while (resultSet.hasNext()) {
								QuerySolution qs = resultSet.next();
								String uri = qs.getResource("dbpedia").getURI();
								dbpediaURL = uri;
								break;
							}
							if(dbpediaURL == null) {
								String[] urlParts = jrcNamesUrl.split("/");
								resourceName = urlParts[urlParts.length-1];
								boolean jrcNamesMatch = this.dbpedia.ask("PREFIX dbpedia: <http://dbpedia.org/resource/> PREFIX owl: <http://www.w3.org/2002/07/owl#> ASK { dbpedia:"+resourceName+" a owl:Thing.}");
								if(jrcNamesMatch) {
									dbpediaURL = "http://dbpedia.org/resource/"+resourceName;
								}
								else {
									System.out.println("Attempting to guess DBpedia label from JRCNames URI...");
									String[] mightBeLabelParts = resourceName.split("_");
									String mightBeLabel = mightBeLabelParts[0];
									for(int j = 1; j < mightBeLabelParts.length; j++) {
										mightBeLabel += " "+mightBeLabelParts[j];
									}
									result = (SparqlResult) this.dbpedia.query("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT ?uri FROM <http://dbpedia.org> WHERE { ?uri rdfs:label ?l FILTER(STR(?name)='"+mightBeLabel+"')}");
									resultSet = result.getResultSet();
									if(resultSet.hasNext()) {
										String uri = this.getBestUrlMatchForResource(resultSet);
										dbpediaURL = uri;
									}
								}
							}
						}
						
						String type = element.getAttribute("type");
						if(dbpediaURL == null) {
							// JRCNames was useless, let's match anything we can find!
							System.out.println("No link to DBpedia found, attempting manual matching...");
							String couldbeResourceName = this.labelToResource(content);
							boolean resourceExists = this.dbpedia.ask("PREFIX owl: <http://www.w3.org/2002/07/owl#> PREFIX dbpedia: <http://dbpedia.org/resource/> ASK { dbpedia:"+couldbeResourceName+" a owl:Thing. }");
							if(resourceExists) {
								dbpediaURL = "http://dbpedia.org/resource/"+couldbeResourceName;
							}
							else {
								SparqlResult result = (SparqlResult) this.dbpedia.query("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT ?uri FROM <http://dbpedia.org> WHERE { ?uri rdfs:label ?l FILTER(STR(?name)='"+content+"')}");
								ResultSet resultSet = result.getResultSet();
								if(resultSet.hasNext()) {
									String uri = this.getBestUrlMatchForResource(resultSet);
									dbpediaURL = uri;
								}
								else {
									if(type.equals("PERSON") || type.equals("LOCATION") || type.equals("ORGANIZATION")) {
										String owlType = null;
										if(type.equals("PERSON")) owlType = "Person";
										else if(type.equals("ORGANIZATION")) owlType = "Organisation";
										else if(type.equals("LOCATION")) owlType = "Place";
										System.out.println("No exact matches could be found. Attempting bruteforce matching.");
										result = (SparqlResult) this.dbpedia.query("PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT ?uri FROM <http://dbpedia.org> WHERE { ?uri rdfs:label ?l FILTER(regex(?l, '"+ content + "', 'i')). ?uri a dbpedia-owl:"+owlType+"}");
										resultSet = result.getResultSet();
										String uri = this.getBestUrlMatchForResource(resultSet);
										if(uri != null) {
											dbpediaURL = uri;
										}
									}
								}
							}
						}
					}
					else {
						dbpediaURL = element.getAttribute("dbpedia");
					}
					if(dbpediaURL != null) {
						element.setAttribute("dbpedia", dbpediaURL);
						System.out.println("Match found: "+dbpediaURL);
						this.storeDBpediaDataInTDB(dbpediaURL);
					}
				}
			}
			catch(QueryParseException e) {
				System.out.println("Parse Error. Continuing.");
			}
		}
	}
	
	protected String getBestUrlMatchForResource(ResultSet resultSet) {
		HashMap<String, Integer> uriCount = new HashMap<String, Integer>();
		ArrayList<String> urls = new ArrayList<String>();
		while (resultSet.hasNext()) {
			QuerySolution qs = resultSet.next();
			String uri = qs.getResource("uri").getURI();
			if(this.knownURIs.contains(uri)) {
				return uri;
			}
			urls.add(uri);
			if(uriCount.containsKey(uri)) {
				uriCount.put(uri, uriCount.get(uri)+1);
			}
			else {
				uriCount.put(uri, 1);
			}
		}
		int maxCount = 0;
		String winnerUri = null;
		for(String uri : uriCount.keySet()) {
			if(uriCount.get(uri) > maxCount) {
				maxCount = uriCount.get(uri);
				winnerUri = uri;
			}
		}
		this.knownURIs.add(winnerUri);
		return winnerUri;
	}

	protected void storeDataInXML(Node sentence, NamedEntity namedEntity) {
		if(namedEntity.nameParts.length > 1) {
			int startIndex = namedEntity.startIndex;
			Element firstNeElement = (Element) this.findNodeByIndex(sentence,
					startIndex);
			for (int i = startIndex + 1; i < startIndex
					+ namedEntity.nameParts.length; i++) {
				this.deleteNodeByIndex(sentence, i);
			}
			firstNeElement.setTextContent(namedEntity.name);
			if(namedEntity.jrcNamesUri != null) {
				firstNeElement.setAttribute("jrc-names", namedEntity.jrcNamesUri);
			}
			if(namedEntity.DBpediaUri != null) {
				firstNeElement.setAttribute("dbpedia", namedEntity.DBpediaUri);
			}
		}
		else {
			Element neElement = (Element) this.findNodeByIndex(sentence, namedEntity.startIndex);
			if(namedEntity.jrcNamesUri != null) {
				neElement.setAttribute("jrc-names", namedEntity.jrcNamesUri);
			}
			if(namedEntity.DBpediaUri != null) {
				neElement.setAttribute("dbpedia", namedEntity.DBpediaUri);
			}
		}
	}
	
	protected void storeDBpediaDataInTDB(String url) {
		System.out.println("Storing dbpedia entry into local triple store.");
		Dataset ds = this.localStore.getDataset();
		ds.begin(ReadWrite.WRITE);
		try {
			Model localDBpedia = ds.getDefaultModel();
			this.dbpedia.describeIntoModel("DESCRIBE <"+url+">", localDBpedia);
			localDBpedia.close();
			ds.commit();
		} finally { 
	        ds.end() ; 
	    }
	}

	protected Node findNodeByIndex(Node sentence, int index) {
		NodeList childs = sentence.getChildNodes();
		for (int childIndex = 0; childIndex < childs.getLength(); childIndex++) {
			Node child = childs.item(childIndex);
			if (child.hasAttributes()) {
				NamedNodeMap attributes = child.getAttributes();
				Node indexAttribute = attributes.getNamedItem("index");
				int nodeIndex = Integer.valueOf(indexAttribute.getNodeValue());
				if (index == nodeIndex) {
					return child;
				}
			}
		}
		return null;
	}

	protected void deleteNodeByIndex(Node sentence, int index) {
		Node node = this.findNodeByIndex(sentence, index);
		Node previousNode = node.getPreviousSibling();
		String previousText = previousNode.getTextContent();
		if (previousText.endsWith(" ")) {
			previousNode.setTextContent(previousText.substring(0,
					previousText.length() - 1));
		}
		sentence.removeChild(node);
	}

	protected ArrayList<NamedEntity> matchEntities(NamedEntity namedEntity) {
		ArrayList<NamedEntity> matchedEntities = new ArrayList<NamedEntity>();
		ResultSet uris = null;
		boolean skipJRCNames = false;
		if(namedEntity.type.equals("PERSON") || namedEntity.type.equals("ORGANIZATION")) {
			uris = this.getURIsFromJrcNames(namedEntity);
		}
		else if(namedEntity.type.equals("LOCATION")) {
			uris = this.getLocationURIsFromDBpedia(namedEntity);
			skipJRCNames = true;
		}
		if (uris == null || !uris.hasNext()) {
			int partCount = namedEntity.nameParts.length;
			if (partCount == 2) {
				matchedEntities.addAll(this.matchEntities(new NamedEntity(
						namedEntity.nameParts[0], namedEntity.startIndex,
						namedEntity.type)));
				matchedEntities.addAll(this.matchEntities(new NamedEntity(
						namedEntity.nameParts[1], namedEntity.startIndex + 1,
						namedEntity.type)));
			} else if (partCount == 3) {
				ArrayList<NamedEntity> r1 = this.matchEntities(new NamedEntity(
						namedEntity.nameParts[0], namedEntity.startIndex,
						namedEntity.type));
				ArrayList<NamedEntity> r2 = this.matchEntities(new NamedEntity(
						namedEntity.nameParts[1] + " "
								+ namedEntity.nameParts[2],
						namedEntity.startIndex + 1, namedEntity.type));
				if (r1.size() == 0 && r2.size() == 0) {
					r1 = this.matchEntities(new NamedEntity(
							namedEntity.nameParts[0] + " "
									+ namedEntity.nameParts[1],
							namedEntity.startIndex, namedEntity.type));
				}
				matchedEntities.addAll(r1);
				matchedEntities.addAll(r2);
			} else if (partCount == 4) {
				ArrayList<NamedEntity> r1 = this.matchEntities(new NamedEntity(
						namedEntity.nameParts[0], namedEntity.startIndex,
						namedEntity.type));
				ArrayList<NamedEntity> r2 = this.matchEntities(new NamedEntity(
						namedEntity.nameParts[1] + " "
								+ namedEntity.nameParts[2] + " "
								+ namedEntity.nameParts[3],
						namedEntity.startIndex + 1, namedEntity.type));
				if (r1.size() == 0 && r2.size() == 0) {
					r1 = this.matchEntities(new NamedEntity(
							namedEntity.nameParts[0] + " "
									+ namedEntity.nameParts[1] + " "
									+ namedEntity.nameParts[2],
							namedEntity.startIndex, namedEntity.type));
				}
				matchedEntities.addAll(r1);
				matchedEntities.addAll(r2);
			}
		} else {
			String disambiguatedUri = this.getBestUrlMatchForResource(uris);
			System.out.println(disambiguatedUri);
			if(skipJRCNames) {
				namedEntity.DBpediaUri = disambiguatedUri;
			}
			else {
				namedEntity.jrcNamesUri = disambiguatedUri;
			}
			matchedEntities.add(namedEntity);
		}
		return matchedEntities;
	}

	protected ResultSet getURIsFromJrcNames(NamedEntity namedEntity) {
		System.out.println(namedEntity.name);
		String type = "Person";
		if (namedEntity.type.equals("ORGANIZATION")) {
			type = "Organisation";
		}
		SparqlResult result = (SparqlResult) this.jrcNames.query("PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT ?uri WHERE { ?uri a dbpedia-owl:"+type+". ?uri rdfs:label \""+ namedEntity.name + "\"}");
		if(!result.getResultSet().hasNext()) {
			String name = namedEntity.name.replace("'", "\\'");
			result = (SparqlResult) jrcNames
					.query("PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT ?uri WHERE { ?uri a dbpedia-owl:"+type+". ?uri rdfs:label ?l FILTER(regex(?l, '"
							+ name + "', 'i'))}");
		}
		return result.getResultSet();
	}
	
	protected String labelToResource(String label) {
		label = label.replace(" ", "_");
		label = label.replace(".", "");
		return label;
	}
	
	protected ResultSet getLocationURIsFromDBpedia(NamedEntity namedEntity) {
		System.out.println(namedEntity.name);
		SparqlResult result = null;
		ResultSet resultSet = null;
		String couldbeResourceName = this.labelToResource(namedEntity.name);
		resultSet = ((SparqlResult) this.dbpedia.query("PREFIX dbpedia: <http://dbpedia.org/resource/> PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> SELECT ?uri FROM <http://dbpedia.org> WHERE { BIND (dbpedia:"+couldbeResourceName+" as ?uri) ?uri a dbpedia-owl:Place}")).getResultSet();
		if(!resultSet.hasNext()) {
			resultSet = ((SparqlResult) this.dbpedia.query("PREFIX dbpedia-owl: <http://dbpedia.org/ontology/> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT ?uri FROM <http://dbpedia.org> WHERE { ?uri a dbpedia-owl:Place. ?uri rdfs:label ?l FILTER(STR(?l) = '"+ namedEntity.name + "'). }")).getResultSet();
		}
		return resultSet;
		
	}

	public void checkForWordBetweenTwoNE()
			throws TransformerConfigurationException {

		String q = "for $s in //s\n" + "return $s";
		int sentences = 0;
		int verbs = 0;
		// System.out.println(this.baseX.query(q).getResultAsString());
		int counter = 0;
		XMLResult xmlResult = (XMLResult) this.linkedArticles.query(q);
		Document dom = xmlResult.getDocument();
		NodeList list = dom.getElementsByTagName("s");

		for (int b = 0; b < list.getLength(); b++) {
			sentences++;
			NodeList childs = list.item(b).getChildNodes();
			// if there are less than 3 nodes, there can exist a combination
			if (childs.getLength() >= 3) {
				for (int a = 0; a < childs.getLength() - 2; a++) {
					Node firstNe = childs.item(a);
					if (firstNe.getNodeName().equals("nm")
							&& (firstNe.getAttributes().getNamedItem("type")
									.getTextContent().equals("PERSON")
									|| firstNe.getAttributes()
											.getNamedItem("type")
											.getTextContent()
											.equals("LOCATION") || firstNe
									.getAttributes().getNamedItem("type")
									.getTextContent().equals("ORGANIZATION"))) {
						Node verb = childs.item(a + 1);
						if (verb.getTextContent() != "#text") {
							Node secondNe = childs.item(a + 2);
							String word = verb.getTextContent().trim();
							if (secondNe.getNodeName().equals("nm")
									&& (secondNe.getAttributes()
											.getNamedItem("type")
											.getTextContent()
											.equals("ORGANIZATION")
											|| firstNe.getAttributes()
													.getNamedItem("type")
													.getTextContent()
													.equals("PERSON")

									|| firstNe.getAttributes()
											.getNamedItem("type")
											.getTextContent()
											.equals("LOCATION"))) {
								// System.out.println("yes");
								try {
									if (!word.equals("")
											&& word.split(" ").length == 1
											&& !word.equals("'s")
											&& !word.equals(",")
											&& !word.equals(",''")
											&& !word.contains("'")) {
//										System.out.println(firstNe
//												.getTextContent() + " "
//												+ word + " " 
//												+ secondNe.getTextContent());
										String[] verbResult = checkWordForPOSType(
												getWholeText(childs), word);
										counter++;

										if (verbResult != null && verbResult[0] != null
												&& verbResult[0].equals("VERB")) {
											
											String firstNamedEntityURI = null;
											String secondNamedEntityURI = null;
											Element firstNamedEntityElement = (Element) firstNe;
											Element secondNamedEntityElement = (Element) secondNe;
											
											if(firstNamedEntityElement.hasAttribute("dbpedia")) {
												firstNamedEntityURI = firstNamedEntityElement.getAttribute("dbpedia");
											}
											else if(firstNamedEntityElement.hasAttribute("jrc-names")) {
												firstNamedEntityURI = firstNamedEntityElement.getAttribute("jrc-names");
											}
											else {
												firstNamedEntityURI = "http://acoli.cs.uni-frankfurt.de/db3/"+this.labelToResource(firstNamedEntityElement.getTextContent());
											}
											
											if(secondNamedEntityElement.hasAttribute("dbpedia")) {
												secondNamedEntityURI = secondNamedEntityElement.getAttribute("dbpedia");
											}
											else if(secondNamedEntityElement.hasAttribute("jrc-names")) {
												secondNamedEntityURI = secondNamedEntityElement.getAttribute("jrc-names");
											}
											else {
												secondNamedEntityURI = "http://acoli.cs.uni-frankfurt.de/db3/"+this.labelToResource(secondNamedEntityElement.getTextContent());
											}
											
											Triple triple = new Triple(firstNamedEntityURI, word, secondNamedEntityURI);
											int[] output = this.addableIntoTriples(triple);
											if(output[0] == 1){
												this.triples.add(triple);
											}
											else{
												this.triples.get(output[1]).incrementCount();
											}
											
											
											verbs++;
										}

									}

								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								// triples++;
							}
						}
					} else {
						// System.out.println("no");
					}
				}
			}
		}
		System.out.println(verbs + " " + counter + " " + sentences+ " test");
	}

	public void checkForWordBeforeNE() throws TransformerConfigurationException {

		String q = "for $s in //s\n" + "return $s";
		int triples = 0;
		// System.out.println(this.baseX.query(q).getResultAsString());
		int adjectives = 0;
		int counter = 0;
		XMLResult xmlResult = (XMLResult) this.linkedArticles.query(q);
		Document dom = xmlResult.getDocument();
		NodeList list = dom.getElementsByTagName("s");

		for (int b = 0; b < list.getLength(); b++) {
			triples++;
			NodeList childs = list.item(b).getChildNodes();
			if (childs.getLength() >= 2) {
				for (int a = 0; a < childs.getLength() - 1; a++) {
					Node word = childs.item(a);
					if (word.getTextContent() != "#text"
							&& (word.getAttributes() == null)
							&& !word.equals(" ")) {
						Node firstNe = childs.item(a + 1);
						// System.out.println(firstNe.getNodeValue());
						if (firstNe.getNodeName().equals("nm")
								&& (firstNe.getAttributes()
										.getNamedItem("type").getTextContent()
										.equals("PERSON")
										|| firstNe.getAttributes()
												.getNamedItem("type")
												.getTextContent()
												.equals("LOCATION") || firstNe
										.getAttributes().getNamedItem("type")
										.getTextContent()
										.equals("ORGANIZATION"))) {

							String wordText = word.getTextContent();
							//System.out.println("org" + wordText);
							// System.out.println(wordText +" " +
							// firstNe.getTextContent() + " "+
							// firstNe.getAttributes().getNamedItem("index").getNodeValue());
							wordText = wordText.replace(",", "");
							if (wordText.split(" ").length > 0) {
								String[] wordParts = wordText
										.split(" ");
								wordText = wordParts[wordParts.length - 1]
										.trim();
							}
							if (!wordText.equals("")
									&& !wordText.contains(",")
									&& !wordText.contains(" ")
									&& !wordText.contains("'")
									&& !wordText.contains("`")
									&& !wordText.contains("(")) {
								//System.out.println(wordText);
								
//								System.out.println(wordText
//										+ " "
//										+ firstNe.getTextContent()
//										+ " "
//										+ firstNe.getAttributes()
//												.getNamedItem("index")
//												.getNodeValue());
								try {
									
									//System.out.println(getWholeText(childs));
									//System.out.println(wordText);
									//System.out.println("_____________");
									String[] adjective = checkWordForPOSType(
											getWholeText(childs), wordText);
									if (adjective != null && adjective[0] != null
											&& (adjective[0].equals("ADJECTIVE"))) {
										System.out.println(adjective[0]);
										
										
										

										String namedEntityURI = null;
										Element namedEntityElement = (Element) firstNe;
										
										if(namedEntityElement.hasAttribute("dbpedia")) {
											namedEntityURI = namedEntityElement.getAttribute("dbpedia");
										}
										else if(namedEntityElement.hasAttribute("jrc-names")) {
											namedEntityURI = namedEntityElement.getAttribute("jrc-names");
										}
										else {
											namedEntityURI = "http://acoli.cs.uni-frankfurt.de/db3/"+this.labelToResource(namedEntityElement.getTextContent());
										}
										Tuple tuple = new Tuple(namedEntityURI, wordText);
										int[] output = this.addableIntoTuples(tuple);
										if(output[0] == 1){
											this.tuples.add(tuple);
										}
										else{
											this.tuples.get(output[1]).incrementCount();
										}
										
										
										
										adjectives++;
									}
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								counter++;
							}
						}
					} else {
						// System.out.println("no");
					}
				}
			}
		}
		System.out.println(adjectives + "   " + counter + "   " + triples);
	}
	
	

	public ArrayList<Integer> getIndeciesOfWordInSentence(String sentence,
			String word) {
		ArrayList<Integer> indeces = new ArrayList<Integer>();
		for (int i = -1; (i = sentence.indexOf(word, i + 1)) != -1;) {
			indeces.add(i);
		}
		return indeces;
	}

	public String[] checkWordForPOSType(String sentence,String word)
			throws MalformedURLException, IOException {
		// sentence =
		// "Sheikholeslam was a member of a delegation accompanying Majlis Speaker Ali Larijani in his visit to China.";
		
		String converted = sentence.replace(" '", "\\'").replace(" ", "%20");
		// System.out.println(sentence);
		// System.out.println(converted);
		//
		//int wordIndex = this.getWordIndex(sentence, word, wordNumber);
		// System.out.println(word);
		String url = "http://nlp2rdf.lod2.eu/demo/NIFStanfordCore?input="
				+ converted + "&input-type=text";
		String sparql = "PREFIX str: <http://nlp2rdf.lod2.eu/schema/string/>"
				+ "PREFIX sso: <http://nlp2rdf.lod2.eu/schema/sso/>"
				+ "SELECT ?type ?lemma ?w WHERE { ?w str:anchorOf '" + word
				+ "'.?w sso:posTag ?type." + "?w sso:lemma ?lemma}";

		Model model = ModelFactory.createDefaultModel();
		InputStream in = new URL(url).openStream();
		if (in == null) {
			throw new IllegalArgumentException("Url: " + url + " not found");
		}

		try{
			model.read(in, null);
		}
		catch(RiotException e){
			System.err.println("EXCEPTIOn");
			System.out.println(word);
			
		}
		//model.write(System.out);

		Query query = QueryFactory.create(sparql);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		String type = null;
		String lemma = null;
		String index = null;
		int start = 0;
		int end = 0;
		try {
			ResultSet results = qe.execSelect();
			while (results.hasNext()) {
				QuerySolution qs = results.nextSolution();
				type = qs.getLiteral("type").toString();
				lemma = qs.getLiteral("lemma").toString();
				index = qs.getResource("w").toString();

//				if (type != null && lemma != null && index != null) {
//					System.out.println("index:" + index);
//					String[] ressourceParts = index.split("_");
//					start = Integer
//							.parseInt(ressourceParts[ressourceParts.length - 2]);
//					if(start == wordIndex){
//						break;
//					}
//				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			qe.close();
		}
//		System.out.println("word: "+word+" type: " + type + " lemma: " +
//		lemma);
		
		
		if (type != null && type.contains("VB")) {
			//System.out.println("VERB");
//			System.out.println("word: "+word+" type: " + type + " lemma: " +
//					lemma);
			return new String[] {"VERB", lemma};
		} else if (type != null && type.contains("JJ")) {
			//System.out.println("ADJECTIVE");
//			System.out.println("word: "+word+" type: " + type + " lemma: " +
//					lemma);
			return new String[] {"ADJECTIVE", lemma};
		} else {
			return null;
		}
	}
	
	public String getWholeText(NodeList list)
			throws TransformerConfigurationException {
		String text = "";
		for (int a = 0; a < list.getLength(); a++) {
			Node node = list.item(a);

			if (node.getTextContent().equals("#text")) {
				
				text += node.getNodeValue();
			} else {
				String textNode = node.getTextContent().trim();
				if(!textNode.equals("")){
					text += node.getTextContent();
				}
			}
		}
		//System.out.println(text);
		return text;
	}
	
	public int getWordIndex(String sentence, String word, int wordNumber){
		String[] sentenceParts = sentence.split(" ");
		ArrayList<String> wholeSentence = new ArrayList<String>();
		String text = "";
		for(String s : sentenceParts){
			if(!s.contains(",")){
				wholeSentence.add(s+" ");
			}
			else{
				if(!s.equals(",")){
					
					wholeSentence.add(s+ " ");
				}
				else{
					if(!s.startsWith(",") && s.split(",").length > 1 && !s.endsWith(",")){
						String[] parts = s.split(",");
						wholeSentence.add(parts[0]);
						wholeSentence.add(",");
						wholeSentence.add(parts[1]);
					}
					else if(s.startsWith(",")){
						String lastEntry = wholeSentence.get(wholeSentence.size()-1);
						lastEntry.substring(1, lastEntry.length()-1);
						wholeSentence.set(wholeSentence.size()-1, lastEntry);
						wholeSentence.add(",");
						wholeSentence.add(s.substring(1));
					}
					else if(s.endsWith(",")){
						String lastEntry = wholeSentence.get(wholeSentence.size()-1);
						lastEntry.substring(0, lastEntry.length()-2);
						wholeSentence.set(wholeSentence.size()-1, lastEntry);
						wholeSentence.add(", ");
						wholeSentence.add(s.substring(1));
						
					}
					
				}
			}
		}
		for(String s : wholeSentence){
			text += s;
		}
		int pointer = -1;
		if(wordNumber == 0){
			pointer = 0;
		}
		else{
			for(int a = 0; a < wordNumber; a++){
				pointer += wholeSentence.get(a).length();
			}
			pointer++;
		}
		
		
		//System.out.println(sentence.substring(110));
//		System.out.println("org: " + sentence);
//		System.out.println("myo: " + text);
		return pointer;
	}
	
	public int[] addableIntoTuples(Tuple tupel){
		for(int a = 0; a < this.tuples.size(); a++){
			Tuple t = this.tuples.get(a);
			String tUri = t.getSubjectUri();
			String tupelUri = tupel.getSubjectUri();
			if(tUri.equals(tupelUri)){
				String tAdjective = t.getAdjective();
				String tupelAjective = tupel.getAdjective();
				if(tAdjective.equals(tupelAjective)){
					return new int[]{0,a};
				}
			}
			else{
				return new int[]{1,0};
			}
		}
		return new int[]{1,0};
	}
	
	public int[] addableIntoTriples(Triple triple){
		for(int a = 0; a < this.triples.size(); a++){
			Triple t = this.triples.get(a);
			String tUri = t.getSubjectUri();
			String tripleUri = triple.getSubjectUri();
			if(tUri.equals(tripleUri)){
				String tWord = t.getVerb();
				String tripleWord = triple.getVerb();
				if(tWord.equals(tripleWord)){
					tUri = t.getObjectUri();
					tripleUri = triple.getObjectUri();
					if(tUri.equals(tripleUri)){
						return new int[]{0,a};
					}
					else{
						return new int[]{1,0};
					}
					
					
				}
				else{
					return new int[]{1,0};
				}
			}
			else{
				return new int[]{1,0};
			}
		}
		return new int[]{1,0};
	}
	
	public void saveIntoStore(){
		Dataset dataset = this.localStore.getDataset();
		try {
			dataset.begin(ReadWrite.WRITE);
			Model model = dataset.getDefaultModel();
			Resource firstNamedEntity = null;
			Resource secondNamedEntity = null;
			
			for(Tuple tupel : this.tuples){
				firstNamedEntity = model.createResource(tupel.getSubjectUri());
				
				Property hasAttribute = model.createProperty("http://acoli.cs.uni-frankfurt.de/db3/hasAttribute");
				
				Resource adjectiveResource = model.createResource("http://acoli.cs.uni-frankfurt.de/db3/"+tupel.getAdjective());
				
				Statement s = model.createStatement(firstNamedEntity, hasAttribute, adjectiveResource);
				
				ReifiedStatement rStatement = model.createReifiedStatement(s);
				
				Property occurrences = model.createProperty("http://acoli.cs.uni-frankfurt.de/db3/occurences");
				
				Statement s2 = model.createLiteralStatement(rStatement, occurrences, tupel.getCount());
			
				model.add(s2);
			}
			
			for(Triple triple : this.triples){
				firstNamedEntity = model.createResource(triple.getSubjectUri());
				secondNamedEntity = model.createResource(triple.getObjectUri());
				
				Property verbResource = model.createProperty("http://acoli.cs.uni-frankfurt.de/db3/"+triple.getVerb());
				
				Statement s = model.createStatement(firstNamedEntity, verbResource, secondNamedEntity);
				
				ReifiedStatement rStatement = model.createReifiedStatement(s);
				
				Property occurrences = model.createProperty("http://acoli.cs.uni-frankfurt.de/db3/occurences");
				
				Statement s2 = model.createLiteralStatement(rStatement, occurrences, triple.getCount());
				
				model.add(s2);
			}
			model.close();
			dataset.commit();
		}
		finally {
			dataset.end();
		}
		
	}

	public static void main(String[] args) throws IOException,
			TransformerConfigurationException {
		EntityLinker linker = new EntityLinker();
		linker.link();
		linker.checkForWordBetweenTwoNE();
        linker.checkForWordBeforeNE();
        linker.saveIntoStore();
        System.out.println("Linking and analysis complete!");
	}

}
