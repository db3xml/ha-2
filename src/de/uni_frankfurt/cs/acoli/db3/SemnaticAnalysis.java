//package de.uni_frankfurt.cs.acoli.db3;
//
//import java.util.List;
//import java.util.Properties;
//import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
//import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
//import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
//import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
//import edu.stanford.nlp.ling.CoreLabel;
//import edu.stanford.nlp.pipeline.Annotation;
//import edu.stanford.nlp.pipeline.StanfordCoreNLP;
//import edu.stanford.nlp.util.CoreMap;
//
//public class SemnaticAnalysis {
//
//	private Properties props;
//	private StanfordCoreNLP pipeline;
//
//	public SemnaticAnalysis() {
//		this.props = new Properties();
//		this.props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
//		this.pipeline = new StanfordCoreNLP(props);
//	}
//
//	public String checkForVerb(String verb) {
//		Annotation document = new Annotation(verb);
//		this.pipeline.annotate(document);
//		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
//		
//		for (CoreMap sentence : sentences) {
//			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
//				String word = token.get(TextAnnotation.class);
//				String pos = token.get(PartOfSpeechAnnotation.class);
//				
//				if (pos.contains("VB")) {
//					System.out.println("part of speach " + pos + "  " + word);
//					return verb;
//				}
//			}
//		}
//		return null;
//	}
//
//	public String checkForAdjective(String adjective) {
//
//		Annotation document = new Annotation(adjective);
//		this.pipeline.annotate(document);
//		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
//
//		for (CoreMap sentence : sentences) {
//			for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
//				String word = token.get(TextAnnotation.class);
//				String pos = token.get(PartOfSpeechAnnotation.class);
//
//				if (pos.contains("JJ")) {
//					System.out.println("part of speach " + pos + "  " + word);
//					return "adjective";
//				}
//			}
//		}
//		return null;
//	}
//	
//	public static void main(String[] args)  {
//		SemnaticAnalysis sA = new SemnaticAnalysis();
//		System.out.println(sA.checkForVerb("say"));
//		System.out.println(sA.checkForAdjective("gay"));
//	}
//
//}