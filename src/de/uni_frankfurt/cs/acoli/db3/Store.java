package de.uni_frankfurt.cs.acoli.db3;

/**
 * Store interface
 * @author alex
 *
 */
public interface Store {

	public Result query(String query);
	
}
