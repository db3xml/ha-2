package de.uni_frankfurt.cs.acoli.db3;

/**
 * Model Class for Person
 * @author alex
 *
 */
public class Person {
	
	protected String name;
	protected NamedEntity[] namedEntities;
	private String uri;
	
	/**
	 * Constructor of the class
	 * @param name name of person
	 * @param uri uri of person
	 * @param namedEntities named entites array of person
	 */
	public Person(String name, String uri, NamedEntity[] namedEntities){
		this.name = name;
		this.uri = uri;
		this.namedEntities = namedEntities;
	}
	
	/**
	 * returns the name
	 * @return String
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * sets the name
	 * @param name name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * returns the Array of NamedEntities
	 * @return NamedEntities[]
	 */
	public NamedEntity[] getNamedEntities() {
		return namedEntities;
	}

	/**
	 * sets the NamedEntites Array
	 * @param namedEntities Array of namedEntites
	 */
	public void setNamedEntities(NamedEntity[] namedEntities) {
		this.namedEntities = namedEntities;
	}
	
	/**
	 * returns the URI
	 * @return String
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * sets the URI
	 * @param uri Uri of person
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	
}
