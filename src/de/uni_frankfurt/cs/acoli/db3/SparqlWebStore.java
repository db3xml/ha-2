package de.uni_frankfurt.cs.acoli.db3;

import org.apache.jena.atlas.web.HttpException;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;

/**
 * Sparql Webstore Class
 * @author alex
 *
 */
public class SparqlWebStore implements DataStore {

	private String url;
	
	/**
	 * constructor of the class
	 * @param url url of the webstore
	 */
	public SparqlWebStore(String url) {
		this.url = url;
	}
	
	/**
	 * query methode 
	 */
	@Override
	public Result query(String query) {
		Query q = QueryFactory.create(query);
	    QueryExecution qexec = QueryExecutionFactory.sparqlService(this.url, query);
	    ResultSet results = null;
	    boolean done = false;
	    while(!done) {
		    try {
		    	results = qexec.execSelect();
		    	done = true;
		    	break;
		    } catch(Exception e) {
		    	System.out.println(query);
		    	e.printStackTrace();
		    }
		    try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    return new SparqlResult(results);
	}
	
	/**
	 * 
	 * @param query
	 * @return boolean
	 */
	public boolean ask(String query) {
		Query q = QueryFactory.create(query);
	    QueryExecution qexec = QueryExecutionFactory.sparqlService(this.url, query);
	    return qexec.execAsk();
	}
	
	/**
	 * 
	 * @param query
	 * @param model
	 * @return Model
	 */
	public Model describeIntoModel(String query, Model model) {
		Query q = QueryFactory.create(query);
	    QueryExecution qexec = QueryExecutionFactory.sparqlService(this.url, query);
	    return qexec.execDescribe(model);
	}

}
