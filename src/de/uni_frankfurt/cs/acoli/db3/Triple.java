package de.uni_frankfurt.cs.acoli.db3;

public class Triple {

	protected String subjectUri;
	protected String verb;
	protected String objectUri;
	protected int count;
	
	/**
	 * constructor of the class
	 * @param subjectUri uri of the subject
	 * @param verb property
	 * @param objectUri uri of the object
	 */
	public Triple(String subjectUri, String verb, String objectUri){
		this.subjectUri = subjectUri;
		this.verb = verb;
		this.objectUri = objectUri;
		this.count = 1;
	}

	/**
	 * @return the subjectUri
	 */
	public String getSubjectUri() {
		return subjectUri;
	}

	/**
	 * @param subjectUri the subjectUri to set
	 */
	public void setSubjectUri(String subjectUri) {
		this.subjectUri = subjectUri;
	}

	/**
	 * @return the verb
	 */
	public String getVerb() {
		return verb;
	}

	/**
	 * @param verb the verb to set
	 */
	public void setVerb(String verb) {
		this.verb = verb;
	}

	/**
	 * @return the objectUri
	 */
	public String getObjectUri() {
		return objectUri;
	}

	/**
	 * @param objectUri the objectUri to set
	 */
	public void setObjectUri(String objectUri) {
		this.objectUri = objectUri;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	
	public void incrementCount(){
		this.count++;
	}
	
}
