package de.uni_frankfurt.cs.acoli.db3;

public class Tuple {

	protected String subjectUri;
	protected String adjective;
	protected int count;
	
	/**
	 * constructor of the class
	 * @param subjectUri uri of the subject
	 * @param adjective
	 */
	public Tuple(String subjectUri, String adjective){
		this.subjectUri = subjectUri;
		this.adjective = adjective;
		this.count = 1;
	}

	/**
	 * @return the subjectUri
	 */
	public String getSubjectUri() {
		return subjectUri;
	}

	/**
	 * @param subjectUri the subjectUri to set
	 */
	public void setSubjectUri(String subjectUri) {
		this.subjectUri = subjectUri;
	}

	/**
	 * @return the adjective
	 */
	public String getAdjective() {
		return adjective;
	}

	/**
	 * @param adjective the adjective to set
	 */
	public void setAdjective(String adjective) {
		this.adjective = adjective;
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	
	public void incrementCount(){
		this.count++;
	}
	
}
