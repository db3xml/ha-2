package de.uni_frankfurt.cs.acoli.db3;

/**
 * Datastore Interface
 *
 */
public interface DataStore {
	
	public Result query(String query);
}
