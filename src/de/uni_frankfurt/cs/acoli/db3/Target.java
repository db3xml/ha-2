package de.uni_frankfurt.cs.acoli.db3;

/**
 * Target enum
 *
 */
public enum Target {
	BASEX, LOCAL, WEB
}
