package de.uni_frankfurt.cs.acoli.db3;

/**
 *
 * Model Class for a NamedEntity.
 *
 */
public class NamedEntity {

	public String name;
	public int startIndex;
	public String[] nameParts;
	public String jrcNamesUri;
	public String DBpediaUri;
	public String type;
	
	/**
	 * Constructor of the class.
	 * @param name the name of the named entity
	 * @param startIndex the startindex of the named entity
	 * @param type the type of the named entity
	 */
	public NamedEntity(String name, int startIndex, String type) {
		this.name = name;
		this.startIndex = startIndex;
		this.nameParts = name.split(" ");
		this.type = type;
	}
	
}
