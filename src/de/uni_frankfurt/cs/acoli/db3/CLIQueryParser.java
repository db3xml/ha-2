package de.uni_frankfurt.cs.acoli.db3;

import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * This class 
 * 
 *
 */
public class CLIQueryParser {

	private TDBStore tdb;
	private Options parseOptions;
	private GnuParser parser;
	
	
	/**
	 * The constructor of the class. Adds help, target and url to the options parameters"
	 */
	public CLIQueryParser() {
		this.parseOptions = new Options();
		this.parseOptions.addOption("h", "help", false, "Print this help message.");
		this.parseOptions.addOption("t", "target", true, "The target of the query.");
		this.parseOptions.addOption("u", "url", true, "The url for a web query.");
		this.parser = new GnuParser();
		
		this.setUpTDB("store");
	}
	
	/**
	 * parses the query string with the result.
	 * 
	 */
	public void parse(String[] args) {
		try {
			CommandLine cmd = this.parser.parse( this.parseOptions, args);
			if(cmd.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "cli", this.parseOptions );
			}
			else {
				if(cmd.hasOption("import")) {

				}
				else {
					Target target = null;
					if(cmd.hasOption("target")) {
						String targetValue = cmd.getOptionValue("target");
						if(targetValue.equals("local")) {
							target = Target.LOCAL;
						}
						else if(targetValue.equals("web")) {
							target = Target.WEB;
						}
						else if(targetValue.equals("basex")) {
							target = Target.BASEX;
						}
						else {
							System.out.println("Invalid value for target.");
							System.exit(1);
						}
					}
					else {
						target = Target.LOCAL;
					}
					
					List<String> remainingArgs = cmd.getArgList();
					if(remainingArgs.size() == 0) {
						System.out.println("No query specified.");
						System.exit(1);
					}
					String query = remainingArgs.get(0);
					
					switch (target) {
					case WEB:
						if(cmd.hasOption("url")) {
							String url = cmd.getOptionValue("url");
							SparqlWebStore store = new SparqlWebStore(url);
							Result r = store.query(query);
							System.out.println(r.getResultAsString());
						}
						else {
							System.out.println("No URL specified for web query.");
							System.exit(1);
						}
						break;
					case LOCAL:
						Result r = this.tdb.query(query);
						System.out.println(r.getResultAsString());
					default:
						break;
					}
				}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	protected void setUpTDB(String path) {
		this.tdb = new TDBStore(path);
		this.tdb.query("SELECT * WHERE { ?s ?p ?o }");
	}
	
	/**
	 * The main method of the class. It instantiates the parser.
	 * 
	 */
	public static void main(String[] args) {
		CLIQueryParser parser = new CLIQueryParser();
		parser.parse(args);
	}
	
}
